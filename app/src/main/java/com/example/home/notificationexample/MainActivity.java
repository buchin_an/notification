package com.example.home.notificationexample;

import android.app.Notification;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.status_wifi)
    TextView statusWifi;
    @BindView(R.id.status_bluetooth)
    TextView statusBluetooth;
    @BindView(R.id.status_airplane)
    TextView statusAirplane;
    private boolean isWifi;
    private boolean isBluetooth;

    private NotificationManager nm;
    private final int NOTIFICATION_ID_AIR = 42;
    private final int NOTIFICATION_ID_WIFI = 41;
    private final int NOTIFICATION_ID_BLUE = 40;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        isWifi = !isDeviceOnWifi(this);
        isBluetooth = isBluetoothEnabled();

        setStatusAirplane(isAirplaneModeOn(this));
        setStatusBluetooth(isBluetooth);
        setStatusWifi(isWifi);

        nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);


    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
        registerReceiver(bluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(wifiReceiver);
        unregisterReceiver(bluetoothReceiver);
    }

    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isBluetooth = !isBluetooth;
            setStatusBluetooth(isBluetooth);
            if (isBluetooth) {
                showNotificationBlue();
            } else nm.cancel(NOTIFICATION_ID_BLUE);
        }
    };
    private BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isWifi = !isWifi;
            setStatusWifi(isWifi);
            if (isWifi) {
                showNotificationWifi();
            } else nm.cancel(NOTIFICATION_ID_WIFI);
        }
    };

    private void setStatusWifi(boolean status) {
        statusWifi.setText(status ? "on" : "off");
    }

    private void setStatusAirplane(boolean status) {
        statusAirplane.setText(status ? "on" : "off");
    }

    private void setStatusBluetooth(boolean status) {
        statusBluetooth.setText(status ? "on" : "off");
    }


    private void showNotificationAirplane() {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentText("Большой брат следит за тобой")
                .setContentTitle("Title")
                .setAutoCancel(true)
                .setTicker("warning")
                .setProgress(0, 0, true)
                .setWhen(System.currentTimeMillis());
        nm.notify(NOTIFICATION_ID_AIR, builder.build());

    }

    private void showNotificationWifi() {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentText("Большой брат подглядывает за тобой")
                .setContentTitle("Title")
                .setAutoCancel(true)
                .setTicker("warning")
                .setProgress(0, 0, true)
                .setWhen(System.currentTimeMillis());
        nm.notify(NOTIFICATION_ID_WIFI, builder.build());

    }

    private void showNotificationBlue() {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentText("Большой брат за тобой")
                .setContentTitle("Title")
                .setAutoCancel(true)
                .setTicker("warning")
                .setProgress(0, 0, true)
                .setWhen(System.currentTimeMillis());
        nm.notify(NOTIFICATION_ID_WIFI, builder.build());

    }

    private static boolean isAirplaneModeOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }

    public static boolean isDeviceOnWifi(final Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi != null && mWifi.isConnectedOrConnecting();
    }

    public boolean isBluetoothEnabled() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter.isEnabled();

    }

}
